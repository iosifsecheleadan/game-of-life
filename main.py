import sys
import time
import random


def delete_last_line():
	""" Removes last line from standard output """

	# cursor up one line
	sys.stdout.write('\x1b[1A')
	# delete last line
	sys.stdout.write('\x1b[2K')


def delete_last_lines(n = 1):
	""" Removes last n lines from stdout """
	while n > 0:
		delete_last_line()
		n -= 1


class GameOfLife:
	""" Python implementation of game of life 
		on a board of limited size
	"""
	
	def __init__(self, width = 80, height = 23, timeout_S = 0.1):
		"""
		For my configuration
		Default terminal window size = 80 x 23
		Maximized terminal window size = 173 x 41
		"""
		self.width = width
		# self.board = [[None] * width] * height
		self.height = height
		self.board = [[None for _ in range(width)] for _ in range(height)]

		self.timeout_S = timeout_S
		
		self.ONE	= '\u2588'
		self.ZERO	= " "

		self.initialize_random()
	
	def print_board(self):
		for i in range(self.height):
			for j in range(self.width):
				if self.board[i][j]: print(self.ONE, end="")
				else: print(self.ZERO, end="")
			print()
	
	def initialize_random(self):
		# reinitialize seed every time
		# random.seed(a=None)
		for i in range(self.height):
			for j in range(self.width):
				r = bool(int(random.random() * 1.11))
				# print((i, j, r), end="")
				self.board[i][j] = r
	
	def get_neighbors(self, pos_i, pos_j):
		n = 0
		for i in range(pos_i-1, pos_i+2):
			if i < 0 or i >= self.height: continue
			for j in range(pos_j-1, pos_j+2):
				if j < 0 or j >= self.width: continue
				if i == pos_i and j == pos_j: continue
			
				if self.board[i][j]: n += 1
		return n

	def next_generation(self):
		for i in range(self.height):
			for j in range(self.width):
				n = self.get_neighbors(i, j)
				if self.board[i][j]:
					# Solitude - 1
					if n <= 1: self.board[i][j] = False
					# Stability - 3
					elif n <= 3: self.board[i][j] = True
					# Overpopulation
					else: self.board[i][j] = False
				# Reproduction - 3
				elif n == 3: self.board[i][j] = True

	def run(self, iterations = 100):
		given_iterations = iterations
		self.next_generation()
		while iterations > 0:
			iterations -= 1
			self.print_board()
			self.next_generation()
			time.sleep(self.timeout_S)
			delete_last_lines(self.height)
		self.print_board()

		i = input("continue?")
		if i == "y": 
			delete_last_lines(self.height + 1)
			self.run(given_iterations)
		elif i == "r":
			delete_last_lines(self.height + 1)
			self.initialize_random()
			self.run(given_iterations)
	
	def print_neighbors(self):
		for i in range(self.height):
			for j in range(self.width):
				n = self.get_neighbors(i, j)
				if n != 0: print(n, end="")
				else: print(" ", end="")
			print()
	
	def print_matrix(self):
		for i in range(self.height):
			for j in range(self.width):
				print(int(self.board[i][j]), end="")
			print()


GameOfLife().run()