# CLI Game of Life

A pure python implementation of the [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). 

Other implementations can be found all around the internet ([example](https://gameoflife.pro/)).

# How to run

Requires python to run. 

```bash
python main.py
```